      let state_btn = 0;
      let url_endpoint = 'http://reuterscalcapp.ap-southeast-1.elasticbeanstalk.com/';
      set_oper = (num = state_btn) => {
        let result = undefined;
        let a_val = $('#a_input').val();
        let b_val = $('#b_input').val();
        if(num!=0){
          $(".btn").eq(state_btn-1).removeClass('active_btn');
          state_btn = num;
          $(".btn").eq(num-1).addClass('active_btn');
          switch(num){
            case 1: result = Number(a_val) + Number(b_val); break;
            case 2: result = a_val - b_val; break;
            case 3: result = a_val * b_val; break;
            case 4: result = a_val / b_val; break;
            case 5: result = Math.pow(a_val,b_val); break;
            default: result = undefined; break;
          }
          console.log(result);
          if (result == Number.POSITIVE_INFINITY || result == Number.NEGATIVE_INFINITY){
            $('#result_input').val("Infinify");
          }else $('#result_input').val(result);
          ex_json(a_val,b_val);
        }
      }
      ex_clound = async () => {
        let a_val = $('#a_input').val();
        let b_val = $('#b_input').val();
        let username = $('#username').val();
        let password = $('#pass').val(); 
        if(username!=''&&password!=''){
          ex_json(a_val,b_val);
          let formData = {'a_val':a_val,'b_val':b_val,'state_btn':state_btn,'username':username,'password':password};
          $.ajax({
            url: url_endpoint+"save",
            type: "post",
            data: formData,
            success: (data) => {
              alert("Success");
            },
            error: (err) =>{
              console.log(err);
              alert("Error please try again.");
            }
          });
        }
        else{
          alert('You must fill username and password.');
        }
      }
      ex_json = (a_val,b_val) => {
        if(a_val!=''&&b_val!=''&&state_btn!=0){
          $("#save_btn").attr('disabled', false);
          $('#save_btn').css("background-color","#007bff");
          $('#save_btn').css("border-color","#007bff");
          $("#save_cloud_btn").prop('disabled', false);
          if(!$('#cloud_check').is(":checked")){
            var str = JSON.stringify({'a_val':a_val,'b_val':b_val,'state_btn':state_btn});
            var dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(str);
            $("#save_btn").attr("href",dataUri);
          }
        }
        else{
          $("#save_btn").attr('disabled', true);
          $('#save_btn').css("background-color","#55a5fa");
          $('#save_btn').css("border-color","#55a5fa");
          $("#save_cloud_btn").prop('disabled', true);
          $('#result_input').val('');
        }
      }
      Load_json = () => {
        if(!$('#cloud_check').is(":checked")){
          let path = $('#file_load').val();
          if((path.split('.')[1]).toUpperCase()=='JSON'){
            path = document.getElementById("file_load").files[0].path;
            $.getJSON(path,function(data){
              $('#a_input').val(data['a_val']);
              $('#b_input').val(data['b_val']);
              set_oper(data['state_btn']);
            });
          }
        }
        else{
          let username = $('#username').val();
          let password = $('#pass').val(); 
          if(username!=''&&password!=''){
            let formData = {'username':username,'password':password};
            $.ajax({
              url: url_endpoint+"load",
              type: "post",
              data: formData,
              success: (data) => {
                $('#a_input').val(data['a_val']);
                $('#b_input').val(data['b_val']);
                set_oper(Number(data['state_btn']));
              },
              error: (err) =>{
                console.log(err);
                alert("Error please try again.");
              }
            });
          }else{
            alert('You must fill username and password.');
          }
        }
      }
      swap_btn = () => {
        if(!$('#cloud_check').is(":checked")){
          $('#cloud_btn').hide();
          $('#local_btn').show();
        }else{
          $('#cloud_btn').show();
          $('#local_btn').hide();
        }
      }